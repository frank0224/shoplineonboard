app.directive('userForm', function() {
  return {
    templateUrl: '/template/users/user_form.html'
  }
})

app.controller('UsersEditController', function ($scope, $http, userService) {
    getUser($scope, $http, userService)
    $scope.isShow = true
    $scope.submitUser = function () {
      if ($scope.userForm.$valid) {
        $scope.response = null
        updateUser($scope, $http, userService)
      } else {
        $scope.response = "Please fill in the required fields";
      }
    }
  }
);

function getUser($scope, $http, userService) {
  userService.getUser().then(function (response) {
    $scope.user = response
    $scope.formData = angular.copy($scope.user)
  }).catch(function (error) {
    console.log(error)
    recordNotFound()
  });
}

function updateUser($scope, $http, userService) {
  userService.updateUser($scope.formData).then(function (response, status, headers, config) {
    $scope.user = angular.copy($scope.formData)
    $scope.isShow = true
  }).catch(function (error) {
    console.log(error)
    recordNotFound()
  });
}
