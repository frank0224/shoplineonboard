
app.service('userService', function($q, $http, $timeout){

  this.getUser = function() {
    let defered = $q.defer()
    $http.get(`/api/v0/users/${userId.value}`).then(function (response, status, headers, config) {
      defered.resolve(response.data)
    }).catch(function (response, status, headers, config) {
      defered.reject(response)
    })    
    return defered.promise
  };  
  
  this.updateUser = function(data) {
    let defered = $q.defer()
    let req = {
      method: 'PUT',
      url: `/api/v0/users/${userId.value}`,
      data: JSON.stringify(data)
    };
    $http(req).then(function (response, status, headers, config) {
      defered.resolve(response.data)
    }).catch(function (response, status, headers, config) {
      defered.reject(response)
    });
    return defered.promise
  };
})