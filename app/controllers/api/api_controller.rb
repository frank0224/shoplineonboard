module Api
  class ApiController < ActionController::API
    rescue_from Mongoid::Errors::DocumentNotFound do
      render json: { error: '404 not found.' }, status: :not_found
    end
  end
end
