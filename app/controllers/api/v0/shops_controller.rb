module Api
  module V0
    # api of shop CRUD
    class ShopsController < Api::V0::BaseController
      before_action :set_user
      before_action :set_shop, only: %i[show update destroy].freeze
      before_action :valid_shop, only: %i[show update destroy].freeze

      # GET /shops/1
      # GET /shops/1.json
      def show
        render template: 'shops/show.json.jbuilder'
      end

      # POST /shops
      # POST /shops.json
      def create
        @shop = @user.build_shop(shop_params)
        if @shop.save
          head :created, location: api_v0_shops_path(@user)
        else
          render json: @shop.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /shops/1
      # PATCH/PUT /shops/1.json
      def update
        if @shop.update(shop_params)
          head :ok, location: api_v0_shops_path(@user)
        else
          render json: @shop.errors, status: :unprocessable_entity
        end
      end

      # DELETE /shops/1
      # DELETE /shops/1.json
      def destroy
        @shop.destroy
        head :no_content
      end

      private

      def set_user
        @user = User.find(params[:id])
      end

      def set_shop
        @shop = @user.shop
      end

      def valid_shop
        raise(Mongoid::Errors::DocumentNotFound.new(Shop, id: params[:id])) if @shop.nil?
      end

      # Only allow a list of trusted parameters through.
      def shop_params
        params.require(:shop).permit(:name)
      end
    end
  end
end
