module Api
  module V0
    # api of user CRUD
    class UsersController < Api::V0::BaseController
      before_action :set_user, except: %i[index create count].freeze

      # GET /users
      # GET /users.json
      def index
        @users = User.all
        render template: 'api/users/index.json.jbuilder'
      end

      # GET /users/1
      # GET /users/1.json
      def show
        render template: 'api/users/show.json.jbuilder'
      end

      # POST /users
      # POST /users.json
      def create
        @user = User.new(user_params)
        if @user.save
          render template: 'api/users/show.json.jbuilder', status: :created
        else
          render json: @user.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /users/1
      # PATCH/PUT /users/1.json
      def update
        if @user.update(user_params)
          head :ok
        else
          render json: @user.errors, status: :unprocessable_entity
        end
      end

      # DELETE /users/1
      # DELETE /users/1.json
      def destroy
        @user.destroy
        head :no_content
      end

      def count
        @count = User.count
        render template: 'api/users/count.json.jbuilder'
      end

      def name
        render template: 'api/users/name.json.jbuilder'
      end

      private

      # Only allow a list of trusted parameters through.
      def user_params
        params.require(:user).permit(
          :first_name, :last_name,
          :age, :gender,
          { address: User::ADDRESS_PARAMS }
        )
      end

      # Use callbacks to share common setup or constraints between actions.
      def set_user
        @user = User.find(params[:id])
      end
    end
  end
end
