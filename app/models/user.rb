class User
  include Mongoid::Document
  include Mongoid::Timestamps

  GENDERS = %w[male female others].freeze
  ADDRESS_PARAMS = %i[country address_1 address_2].freeze

  field :first_name, type: String
  field :last_name, type: String
  validates :first_name, :last_name, presence: true

  field :gender, type: String
  validates :gender, inclusion: { in: GENDERS }

  field :age, type: Integer
  validates :age, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 0
  }

  field :address, type: Hash

  has_one :shop
end
