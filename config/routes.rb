Rails.application.routes.draw do
  root 'welcome#index'
  resources :users, only: :show
  
  namespace :api do
    namespace :v0 do
      resources :users , except: [:new, :edit] do

        collection do
          get :count
        end

        member do
          get :name
          resource :shops, except: [:index, :new, :edit]
        end
        
      end
    end
  end
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
