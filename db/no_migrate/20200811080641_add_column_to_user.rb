class AddColumnToUser < ActiveRecord::Migration[5.2]
  def up
    add_column :users, :age, :integer
    add_column :users, :gender, :integer
    User.find_each do |user|
      age = rand(18..50)
      gender = rand(0..2)
      user.update(age: age, gender: gender)
    end
  end

  def down
    remove_column :users, :age
    remove_column :users, :gender
  end
end
